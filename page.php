<?php get_header(); ?>

	<section class="main">
		<div class="wrapper">
		
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<article>
					<section id="article-header">
						<h1 class="page-title"><?php the_title(); ?></h1>
					</section>

					<section class="article-body">
						<?php the_content(); ?>
					</section>					
				</article>

			<?php endwhile; endif; ?>

		</div>
	</section>
		   	
<?php get_footer(); ?>