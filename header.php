<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width" />

	<link rel="stylesheet" href="https://indestructibletype.com/fonts/Besley.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>"><img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<nav>
				<div class="nav-wrapper">

					<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
					     <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
					        <?php the_sub_field('label'); ?>	
					     </a>
					<?php endwhile; endif; ?>

				</div>
			</nav>

		</div>
	</header>