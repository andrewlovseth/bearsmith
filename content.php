<article class="standard">
	<div class="wrapper">

		<?php get_template_part('partials/article-header'); ?>

	    <div class="article-body">

	        <?php the_content(); ?>        

	    </div>

	</div>
    
</article>

