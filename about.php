<?php

/*

	Template Name: About

*/


get_header(); ?>

	<section id="about">
		<div class="wrapper">

			<section class="featured-photo">
				<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		
				<div class="caption">
					<p><?php the_field('caption'); ?></p>
				</div>
			</section>

			<section class="biography">
				<?php the_field('biography'); ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>