<?php $featured_posts = get_sub_field('posts'); if( $featured_posts ): ?>

	<section class="featured">

		<?php foreach( $featured_posts as $p ): ?>

			<article class="post post-featured">
				<div class="image">
					<a href="<?php echo get_permalink( $p->ID ); ?>">
						<img src="<?php $image = get_field('featured_photo', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="info">
					<h5 class="tag">
						<a href="<?php $term = get_field('primary_tag', $p->ID); echo get_term_link($term); ?>">
							<?php echo $term->name; ?>
						</a>
					</h5>

					<h3><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h3>

					<?php the_field('teaser', $p->ID); ?>
				</div>
			</article>

		<?php endforeach; ?>

	</section>

<?php endif; ?>


