<?php $the_month = get_the_time( 'Y-m' ); if ( $last_month !== $the_month ): ?>

	<?php if($last_month !== null): ?>
		</section></div>
	<?php endif; ?>

	<?php if(get_the_time('Y') !== $display_year): ?>
		<div class="year-header">
			<h4><?php $display_year = get_the_time('Y'); the_time('Y'); ?></h4>
		</div>
	<?php endif; ?>

	<div class="month month-<?php echo $the_month; ?>">
		<aside class="month-header">
			<?php if(get_the_time('F') !== $display_month): ?>
				<h5 class="month-header"><?php $display_month = get_the_time('F'); the_time('F'); ?></h5>
			<?php endif; ?>		
		</aside>

		<section class="posts">

<?php endif; $last_month = $the_month; ?>

	<?php if(in_category('film-review')): ?>

		<?php get_template_part('partials/blog/film-review'); ?>

	<?php elseif(in_category('feature')): ?>

		<?php get_template_part('partials/blog/feature'); ?>

	<?php else: ?>

		<?php get_template_part('partials/blog/post'); ?>

	<?php endif; ?>

<?php if ( $query->current_post + 1 === $query->post_count ): ?>
	</div>
<?php endif; ?>