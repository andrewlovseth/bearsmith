<article class="post film-review">
	<div class="info">
		<div class="headline">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

			<div class="stars">
				<?php
					$rating = get_field('stars');
					$integer_rating = intval($rating * 10);

					for ($i = 10; $i <= 50; $i = $i + 10) {
						if($i <= $integer_rating) {
							echo '<div class="star full"></div>';
						} elseif($i - 5 == $integer_rating) {
							echo '<div class="star half"></div>';
						}
					}
				?>					
			</div>
		</div>

		<?php if(has_excerpt()): ?>
			<div class="deck">
				<?php the_excerpt(); ?>									
			</div>
		<?php endif; ?>
	</div>

	<div class="date">
		<em><?php the_time('j M'); ?></em>
	</div>
</article>