<article class="post">
	<div class="info">
		<div class="headline">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<?php if(has_excerpt()): ?>
			<div class="deck">
				<?php the_excerpt(); ?>									
			</div>
		<?php endif; ?>
	</div>

	<div class="date">
		<em><?php the_time('j M'); ?></em>
	</div>
</article>