<?php get_header(); ?>

	<article class="issue">
		<div class="wrapper">

			<section class="article-header">
				<h2>404 Error: Page Not Found</h2>
			</section>

			<section class="main-content">

				<h3><strong>Uh oh</strong>. We couldn't find what you were looking for.</h3>

				<p>Here are some handy links to explore:</p>

				<?php $lastestPost = get_posts( 'numberposts=1' ); ?>




				<ul>
					<li>Our most recent issue: <a href="<?php echo get_permalink($lastestPost[0]->ID); ?>"><em><?php echo get_the_title($lastestPost[0]->ID); ?></em></a></li>
					<li>Our <a href="<?php echo site_url('/archives/'); ?>">archives</a></li>
					<li>Our <a href="<?php echo site_url('/about/'); ?>">about page</a></li>
				</ul>

			</section>

		</div>
	</article>

	   	
<?php get_footer(); ?>