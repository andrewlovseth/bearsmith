<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="hero">
		<div class="wrapper">

			<h1><?php the_field('hero_headline'); ?></h1>

			<h2><?php the_field('hero_deck'); ?></h2>

		</div>
	</section>

	<section class="work-showcase">
		<div class="wrapper">

			<?php  if(have_rows('work_showcase')): ?>

				<div class="showcase-grid">

					<?php
						$counter = 1;
						while(have_rows('work_showcase')): the_row(); 
						$showcaseImage = get_sub_field('image');
						$post_object = get_sub_field('project');
						if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<div class="project project-<?php echo $counter; ?>">
								<a href="<?php the_permalink(); ?>">
									<img src="<?php echo $showcaseImage['url']; ?>" alt="<?php echo $showcaseImage['alt']; ?>" />

									<div class="info">
										<div class="info-wrapper">
											<h3><?php the_title(); ?></h3>
											<h4><?php the_field('date'); ?></h4>
										</div>
									</div>
								</a>
							</div>

						<?php wp_reset_postdata(); endif; ?>

					<?php $counter++; endwhile; ?>
					
				</div>
			
			<?php endif; ?>

		</div>
	</section>

	<section class="pricing">
		<div class="wrapper">

			<h3 class="section-header"><?php the_field('pricing_headline'); ?></h3>

			<div class="pricing-grid">
				<?php if(have_rows('pricing_table')): while(have_rows('pricing_table')): the_row(); ?>
				 
				    <div class="option">
				    	<div class="name">
				    		<h4><?php the_sub_field('name'); ?></h4>
				    	</div>

				    	<div class="features">
				    		<?php the_sub_field('features'); ?>
				    	</div>

				    	<div class="price">
				    		<h4><?php the_sub_field('price'); ?></h4>
				    	</div>
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

	<section class="weblog">
		<div class="wrapper">
			
			<section class="archive">

				<?php
					$last_month = null;
					$last_year = null;
					$display_year = null;
					$display_month = null;

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 5
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php include(locate_template('partials/blog/archive.php', false, false)); ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>				

			</section>

		</div>
	</section>

<?php get_footer(); ?>