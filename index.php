<?php get_header(); ?>

	<section class="blog">
		<div class="wrapper">

			<section class="archive">

				<?php
					$last_month = null;
					$last_year = null;
					$display_year = null;
					$display_month = null;

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => -1
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php include(locate_template('partials/blog/archive.php', false, false)); ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>				

			</section>

		</div>
	</section>

<?php get_footer(); ?>