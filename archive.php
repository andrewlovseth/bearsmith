<?php get_header(); ?>

	<section id="blog">
		<div class="wrapper">

			<?php if ( have_posts() ): ?>

				<section id="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<?php $year = get_the_time('Y', '', '', FALSE); if ($year !== $year_check): ?>
							<h2 class="year"><?php the_date('Y'); ?></h2>
						<?php endif; $year_check = $year; ?>

						<?php $month = get_the_time('Y-m', '', '', FALSE); if ($month !== $month_check): ?>
							<h3 class="month"><?php the_time('M'); ?></h3>
						<?php endif; $month_check = $month; ?>

						<article class="weight-<?php the_field('weight'); ?>">

							<section class="article-title">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</section>

							<section class="article-meta">
								<span class="date">
									<span class="month-day"><?php the_time('F j,'); ?></span>
								    <span class="year"><?php the_time('Y'); ?></span>
								</span>
								
							</section>
						
						</article>

					<?php endwhile; ?>

				</section>

			<?php endif; ?>

		</div>

	</section>

<?php get_footer(); ?>