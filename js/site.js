$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').on('click', function() {
		window.open( $(this).attr('href') );
		return false;
	});


	// Mobile Nav Toggle
	$('#toggle').on('click', function() {
		$('header').toggleClass('open');
		$('nav').fadeToggle(300);
		return false;
	});


	// FitVids in Blog
	$('.wp-block-embed__wrapper').fitVids();
 
});