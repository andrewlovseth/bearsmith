<?php get_header(); ?>

	<article>	
		<div class="wrapper">

			<section class="article-body">

				<div class="screenshot">
					<?php if(!get_field('inactive')): ?>
						<a href="<?php the_field('url'); ?>" rel="external">
							<img src="<?php $image = get_field('screenshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					<?php else: ?>
						<img src="<?php $image = get_field('screenshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
				</div>

				<div class="info">
					<h1 class="page-title"><?php the_title(); ?></h1>

					<?php if(get_field('description')): ?>
						<div class="description">
							<?php the_field('description'); ?>
						</div>
					<?php endif; ?>

					<div class="details">

						<?php if(get_field('date')): ?>
							<div class="details--item date">
								<h4>Date</h4>
								<p><?php the_field('date'); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_field('client')): ?>
							<div class="details--item client">
								<h4>Client</h4>
								<p><?php the_field('client'); ?></p>
							</div>
						<?php endif; ?>

						<?php $services = get_field('services'); if($services): ?>
							<div class="details--item services">
								<h4>Services</h4>
								<p>
									<?php foreach($services as $service): ?>
										<span class="service <?php echo sanitize_title_with_dashes($service); ?>"><?php echo $service; ?></span>									
									<?php endforeach; ?>
								</p>
							</div>
						<?php endif; ?>

						<?php if(!get_field('inactive')): ?>
							<div class="details--item view">
								<a href="<?php the_field('url'); ?>" rel="external" class="btn">View Site</a>
							</div>
						<?php endif; ?>

					</div>
				</div>

			</section>

		</div>
	</article>


<?php get_footer(); ?>