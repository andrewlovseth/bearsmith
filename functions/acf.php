<?php

/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/



function my_relationship_query( $args, $field, $post_id ) {

    $args['orderby'] = 'date';
    $args['order'] = 'DESC';

    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');
