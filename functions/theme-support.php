<?php

/*
	Theme Support
*/


// Remove Admin bar from front-end
show_admin_bar( false );

// Theme Support for title tags, post thumbnails, HTML5 elements, feed links
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));
add_theme_support( 'automatic-feed-links' );

// Add wp_body_open
if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}

wp_enqueue_style('font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css");