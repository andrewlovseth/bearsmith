<?php

/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );



/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );



function order_cpt( $query ) {
    if ( !is_admin() && $query->is_post_type_archive('work') &&  $query->is_main_query() ) {
        $query->set( 'orderby', array( 'menu_order' => 'ASC', 'date' => 'ASC' ) );
        $query->set( 'posts_per_page', '100' );
    }
}
add_action( 'pre_get_posts', 'order_cpt'); 